# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the akonadi-calendar package.
#
# Kishore G <kishore96@gmail.com>, 2022.
msgid ""
msgstr ""
"Project-Id-Version: akonadi-calendar\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-09-26 00:45+0000\n"
"PO-Revision-Date: 2022-08-07 11:49+0530\n"
"Last-Translator: Kishore G <kishore96@gmail.com>\n"
"Language-Team: Tamil <kde-i18n-doc@kde.org>\n"
"Language: ta\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 22.07.90\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "கோ. கிஷோர்"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "Kde-l10n-ta@kde.org"

#: alarmnotification.cpp:71
#, kde-format
msgid "View"
msgstr "காட்டு"

#: alarmnotification.cpp:75
#, kde-format
msgid "Task"
msgstr "பணி"

#: alarmnotification.cpp:75
#, kde-format
msgid "Event"
msgstr "நிகழ்வு"

#: alarmnotification.cpp:78
#, kde-format
msgid "Task due at %1"
msgstr "பணிக்குரிய நேரம் %1"

#: alarmnotification.cpp:82
#, kde-format
msgctxt "Event starts in 5 minutes"
msgid "%2 starts in %1 minute"
msgid_plural "%2 starts in %1 minutes"
msgstr[0] "%2 %1 நிமிடத்தில் தொடங்குகிறது"
msgstr[1] "%2 %1 நிமிடங்களில் தொடங்குகிறது"

#: alarmnotification.cpp:86
#, kde-format
msgctxt "Event starts at 10:00"
msgid "%1 starts at %2"
msgstr "%1 %2-க்கு தொடங்குகிறது"

#: alarmnotification.cpp:88
#, kde-format
msgctxt "Event started at 10:00"
msgid "%1 started at %2"
msgstr "%1 %2-க்கு தொடங்கியது"

#: alarmnotification.cpp:94
#, kde-format
msgctxt "Event starts on <DATE> at <TIME>"
msgid "%1 starts on %2 at %3"
msgstr "%1 %2 அன்று %3-க்கு தொடங்குகிறது"

#: alarmnotification.cpp:100
#, kde-format
msgctxt "Event started on <DATE> at <TIME>"
msgid "%1 started on %2 at %3"
msgstr "%1 %2 அன்று %3-க்கு தொடங்குயது"

#: alarmnotification.cpp:109
#, kde-format
msgctxt "Event starts on <DATE>"
msgid "%1 starts on %2"
msgstr "%1 %2 அன்று தொடங்குகிறது"

#: alarmnotification.cpp:111
#, kde-format
msgctxt "Event started on <DATE>"
msgid "%1 started on %2"
msgstr "%1 %2 அன்று தொடங்கியது"

#: alarmnotification.cpp:131
#, kde-format
msgid "Remind in 5 mins"
msgstr "5 நிமிடங்களில் நினைவூட்டு"

#: alarmnotification.cpp:131
#, kde-format
msgid "Remind in 1 hour"
msgstr "1 மணிநேரத்தில் நினைவூட்டு"

#: alarmnotification.cpp:131
#, kde-format
msgctxt "dismiss a reminder notification for an event"
msgid "Dismiss"
msgstr "மறை"

#: alarmnotification.cpp:202
#, kde-format
msgid "Open URL"
msgstr "முகவரியை திற"

#: alarmnotification.cpp:220
#, kde-format
msgid "Map"
msgstr "நிலப்படம்"

#: kalendaracmain.cpp:26
#, kde-format
msgctxt "@title"
msgid "Reminders"
msgstr "நினைவூட்டல்கள்"

#: kalendaracmain.cpp:29
#, kde-format
msgid "Calendar Reminder Service"
msgstr "நாள்காட்டி நினைவூட்டல் சேவை"

#: kalendaracmain.cpp:33
#, kde-format
msgid "(c) KDE Community 2021-2022"
msgstr "(c) கே.டீ.யீ. சமூகம் 2021-2022"

#: kalendaracmain.cpp:34
#, kde-format
msgctxt "@info:credit"
msgid "Carl Schwan"
msgstr "கார்ல் ஷுவான்"

#: kalendaracmain.cpp:35 kalendaracmain.cpp:39
#, kde-format
msgctxt "@info:credit"
msgid "Maintainer"
msgstr "பராமரிப்பாளர்"

#: kalendaracmain.cpp:38
#, kde-format
msgctxt "@info:credit"
msgid "Clau Cambra"
msgstr "க்லாவ் காம்பிரா"
